#ifndef _MAINSCENE_H_
#define _MAINSCENE_H_

#include "cocos2d.h"
using namespace cocos2d;

enum class GameState
{
  START,
  ANIMATE,
  USER_INPUT,
  OVER
};

enum class GameMode
{
  RHYTHM,
  FLASH,
  CHAIN
};

struct RhythmChainState
{
  unsigned int currentTile;
  std::vector<unsigned int> pattern;
}

struct FlashState
{
  std::unordered_set<unsigned int> picture;
}

class MainLayer : public LayerColor
{
  Sprite* grid[16];
  GameState currentState;
  GameMode currentMode;
  union {
    RhythmChainState rcState;
    FlashState fState;
  };
public:
  virtual bool init();
  virtual void update(float dt);
  virtual bool onTouchBegan(Touch* touch, Event* event);
  virtual void onTouchMoved(Touch* touch, Event* event);
  virtual void onTouchEnded(Touch* touch, Event* event);
  static Scene* createScene();
  CREATE_FUNC(MainLayer);
};

#endif

