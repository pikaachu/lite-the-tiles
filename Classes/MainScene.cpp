#include "MainScene.h"

Scene* MainLayer::createScene()
{
  auto scene = Scene::create();
  auto layer = MainLayer::create();
  scene->addChild(layer);
  return scene;
}

Point calcPoint(int row, int col)
{
  int x = row * 200 + 235;
  int y = col * 200 + 600;
  return Point(x, y);
}

void selectTile(Sprite* sprite)
{
  sprite->setTexture(TextureCache::getInstance()->addImage("tile_selected.png"));
}

void unselectTile(Sprite* sprite)
{
  sprite->setTexture(TextureCache::getInstance()->addImage("tile.png"));
}

bool MainLayer::init()
{
  if (!LayerColor::initWithColor(Color4B(44, 62, 80, 255)))
    return false;

  currentMode = GameMode::RHYTHM;
  currentState = GameState::START;

  for (int i = 0; i < 16; i++) {
    grid[i] = Sprite::create("tile.png");
    grid[i]->setPosition(calcPoint(i % 4, i / 4));
    this->addChild(grid[i]);
  }

  auto dispatcher = Director::getInstance()->getEventDispatcher();
  auto listener = EventListenerTouchOneByOne::create();

  listener->onTouchBegan = CC_CALLBACK_2(MainLayer::onTouchBegan, this);
  listener->onTouchMoved = CC_CALLBACK_2(MainLayer::onTouchMoved, this);
  listener->onTouchEnded = CC_CALLBACK_2(MainLayer::onTouchEnded, this);

  dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
  
  return true;
}

void MainLayer::update(float dt)
{
  
}

bool MainLayer::onTouchBegan(Touch* touch, Event* event)
{
  auto currentTime = std::clock();
  auto waitTime = currentTime + 500;

    selectTile(grid[15]);

  return true;
}

void MainLayer::onTouchMoved(Touch* touch, Event* event)
{
}

void MainLayer::onTouchEnded(Touch* touch, Event* event)
{
}
